function ExactMatchBoost() {
	
	// public functions
	this.init = function() {
		if((AJS.Meta.get("search-cql") != null) || (AJS.Meta.get("search-start-index") != null)) {
			startObserver();
		}
	};

	
	// start the mutation observer
	var startObserver = function() {
		var container = document.documentElement || document.body;
		exactMatchBoostObserver.observe(container, exactMatchBoostObserverConfig);
		console.log("Exact Match Boost: starting mutation observer!");
	};
	
	
	// options for observing the DOM changes we need to know
	var exactMatchBoostObserverConfig = {
		childList : true,
		subtree : true
	};
	
	
	// the mutation observer
	var exactMatchBoostObserver = new MutationObserver(function(mutations) {
		mutations.forEach(function(mutation) {
			for(var n = 0; n < mutation.addedNodes.length; n++) {
				
				// help dialog inserted
				if(mutation.addedNodes[n].id == "search-help-dialog") {
					console.log("Exact Match Boost: help dialog inserted in DOM.")
					var helpAdd = AJS.I18n.getText("de.edrup.confluence.plugins.exact-match-boost.help.add");
					if(AJS.$(mutation.addedNodes[n]).html().indexOf(helpAdd) == -1) {
						var chalkAndCheese = AJS.$("pre:contains('chalk and cheese')");
						AJS.$(helpAdd).insertAfter(chalkAndCheese);
					}
				}
				
				// TODO: search results inserted - ask server for better excerpt in case the current excerpt does not contain the exact match
			}
		});
	});
}

var exactMatchBoost = new ExactMatchBoost();

AJS.toInit(function(){
	exactMatchBoost.init();	
});