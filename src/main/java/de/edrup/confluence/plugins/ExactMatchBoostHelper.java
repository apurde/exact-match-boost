package de.edrup.confluence.plugins;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.upm.api.license.PluginLicenseManager;


@Named
public class ExactMatchBoostHelper {
	
	private final BandanaManager bandanaMan;
	private final PluginLicenseManager pluginLicenseMan;
	private final String SETTINGS_KEY = "de.edrup.confluence.plugins.exact-match-boost.settings.v1";
	
	private static final Logger log = LoggerFactory.getLogger(ExactMatchBoostHelper.class);

	
	
	@Inject
	public ExactMatchBoostHelper(@ComponentImport BandanaManager bandanaMan, @ComponentImport PluginLicenseManager pluginLicenseMan) {
		this.bandanaMan = bandanaMan;
		this.pluginLicenseMan = pluginLicenseMan;
	}
	
	
	// return the settings or default in case they don't exist
	public ExactMatchBoostSettings getSettings() {
		return (bandanaMan.getValue(new ConfluenceBandanaContext(), SETTINGS_KEY) != null) ? new ExactMatchBoostSettings((ExactMatchBoostSettings) bandanaMan.getValue(new ConfluenceBandanaContext(), SETTINGS_KEY)) : (new ExactMatchBoostSettings());
	}
	
	
	// set the settings
	public void setSettings(ExactMatchBoostSettings settings) {
		bandanaMan.setValue(new ConfluenceBandanaContext(), SETTINGS_KEY, settings);
	}
	
	
	// return the settings or default in case they don't exist
	public ExactMatchBoostSettings getSettingsRespectingLicense() {
		ExactMatchBoostSettings settings = getSettings();
		boolean validLicense = pluginLicenseMan.getLicense().isDefined() ? pluginLicenseMan.getLicense().get().isValid() : false;
		if(!validLicense) {
			settings.boostFactor = 1.01f;
			log.warn("No valid license found for Exact Match Boost - disabling boosting.");;
		}
		return settings;
	}
}
