package de.edrup.confluence.plugins;

import java.io.Serializable;

public class ExactMatchBoostSettings implements Serializable {

	private static final long serialVersionUID = 1L;
	public float boostFactor = 6.0f;
	public boolean scanAttachments = false;
	public float boostIncrease = 0.0f;
	
	public ExactMatchBoostSettings() {
		boostFactor = 6.0f;
		scanAttachments = false;
		boostIncrease = 0.0f;
	}
	
	public ExactMatchBoostSettings(ExactMatchBoostSettings clone) {
		boostFactor = clone.boostFactor;
		scanAttachments = clone.scanAttachments;
		boostIncrease = clone.boostIncrease;
	}
		
	public Float getBoostFactor() {
		return boostFactor;
	}
	
	public Boolean getScanAttachments() {
		return scanAttachments;
	}
	
	public Float getBoostIncrease() {
		return boostIncrease;
	}
}
